Fanap Task Test

1 - download repository

2- install lumen run project 

3- create database and change .env DB_DATABASE

4- install lumen passport :
composer require dusterio/lumen-passport


5- Next, migrate and install Laravel Passport:

    # Create new tables for Passport
    php artisan migrate

    # Install encryption keys and other necessary stuff for Passport
    php artisan passport:install

6- Create admin and Role with seeder :

    php artisan db:seed --class=RoleTableSeeder
    php artisan db:seed --class=UserTableSeeder

    #username and Password For Admin
    "password":"admin-secret",
    "username":"admin@admin.com",

7- run server : php -S localhost:8000 -t public

8- list of routes:

    # Register User with user roles
    $router->post('/register','RegisterControllers@register') 

    Route::group(['middleware'=>'auth:api'],function (){
        # Active user 
        Route::post('/active-user/{user_id}','ActiveController@change_user_profile');

        #Chane Role 
        Route::post('/change-role/{user_id}','RolesController@change_user_profile');

        #Get Users List
        Route::get('/users-list/','UsersController@index');

        #Add Permissionto role
        Route::post('/add-permission/{role_id}','PermissionsController@add_permission');
    });


9 - After Install Passport and get client secret, to login with postman you should follow the steps below:
    url for login test: localhost:8000/v1/oauth/token

    and for login body you should:

    {
    "grant_type":"password",
    "client_id":[Client_id_that_get_when_install_passport],
    "client_secret":[Client_secret_that_get_when_install_passport],
    "password":"admin-secret",
    "username":"admin@admin.com",
    "scoup":"" 
    }
        

10: after login for access to routes you should uses access_token for authorization into header

