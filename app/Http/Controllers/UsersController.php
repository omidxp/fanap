<?php


namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Facades\Gate;

class UsersController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        if (Gate::allows('admin',$user->id)) {
            $user = User::all();
            return response()->json($user);
        } else {
            return response()->json(['message' => 'شما اجازه دسترسی به این آدرس را ندارید']);
        }
    }
}
