<?php


namespace App\Http\Controllers;


use App\Interfaces\UserInterface;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;


class ActiveController extends Controller implements UserInterface
{

    /**
     * Active User Profile
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function change_user_profile($user_id)
    {
        $user_check = auth()->user();
        $user_to_active= User::find($user_id);

        if ($user_to_active!=null){
            if (Gate::allows('admin', $user_check->role_id)) {
                $change = DB::table('users')->where('id', $user_to_active->id)->update(['status' => '1']);
                return response()->json(['message' => 'کاربر مورد نظر فعال شد']);
            }else{
                return response()->json(['message' => 'عملیات با خطا مواجه شد']);
            }
        }else{
            return response()->json(['message' => 'کابر مورد نظر پیدا نشد']);
        }
    }
}
