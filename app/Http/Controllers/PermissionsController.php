<?php


namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class PermissionsController extends Controller
{
    public function add_permission(Request $request,$role_id)
    {

        $user = auth()->user();
        if (Gate::allows('admin', $user->id)) {
            $result = $request->all();
            $validate = Validator::make($result, [
                'permission_name' => 'required|string'
            ]);
            if ($validate->fails()) {
                return response()->json($validate->errors());
            } else {
                DB::table('permissions')->insert([
                    'name'=>$request->permission_name,
                    'roles_id'=>$role_id
                ]);
                return  response()->json(['message'=>'عملیات با موفقیت انجام شد و دسترسی جدید ایجاد شد']);
            }
        } else {
            return response()->json(['message' => 'شما اجازه دسترسی به این آدرس را ندارید']);
        }
    }
}
