<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use Illuminate\Support\Facades\Route;

$router->post('/register','RegisterControllers@register');

Route::group(['middleware'=>'auth:api'],function (){
    Route::post('/active-user/{user_id}','ActiveController@change_user_profile');
    Route::post('/change-role/{user_id}','RolesController@change_user_profile');
    Route::get('/users-list/','UsersController@index');
    Route::post('/add-permission/{role_id}','PermissionsController@add_permission');
});
